FROM python:alpine

LABEL maintainer "manoj.meda@gmail.com"

WORKDIR /app

ADD . .

RUN pip install -r requirements.txt

CMD ["python"]
